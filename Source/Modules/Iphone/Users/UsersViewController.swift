//
//  UsersViewController.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

import AppRouter
import DTTableViewManager
import RxSwift
import RxCocoa

class UsersViewController: BaseViewController, DTTableViewManageable {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: UsersViewModel!
    
    //MARK: - Lifecicle Object
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAppearance()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let viewModel = viewModel else { return }
        setupTableManager()
        viewModel.load()
    }
    
    //MARK: - Methods Public
    
    override func observeViewModel() {
        viewModel.successHandler = { [weak self] in
            self?.updateUI()
        }
        
        viewModel.errorHandler = { error in
            Alerts.defaultAlert(message: error.localizedDescription)
        }
    }
    
    override func observeState() {
        viewModel.state.asDriver().drive(onNext: { [weak self] state in
            self?.view.progressFor(state: state)
        }).disposed(by: disposeBag)
    }
    
    private func setupTableManager() {
        enableSelfSizing()
        manager.startManaging(withDelegate: self)
        manager.register(UsersCell.self)
        manager.didSelect(UsersCell.self) { [weak self] (_, viewModel, _) in
            viewModel.user.flatMap { self?.open(user: $0) }
        }
        manager.willDisplay(UsersCell.self) { [weak self] (cell, model, indexPath) in
            self?.viewModel?.willDisplay(row: indexPath.row)
        }
        
        canEdit()
    }
    
    private func canEdit()  {
        guard let viewModel = viewModel as? UsersStoredViewModel else { return  }
        
        manager.trailingSwipeActionsConfiguration(for: UsersCell.self) { [weak viewModel, weak self] (cell, model, indexPath) -> UISwipeActionsConfiguration? in
            return self?.createAction(user: model.user, indexPath: indexPath, viewModel: viewModel)
        }
    }
    
    private func createAction(user: User?,
                              indexPath: IndexPath,
                              viewModel: UsersStoredViewModel?)  -> UISwipeActionsConfiguration?
    {
        let actionHandler: UIContextualAction.Handler = { [weak self] (action, _, _) in
            self?.tableView.isEditing = false
            user.map {
                viewModel?.remove(user: $0, at: indexPath.row)
                self?.manager.memoryStorage.removeItems(at: [indexPath])
            }
        }
        
        let action = UIContextualAction.init(style: .normal, title: "Delete", handler: actionHandler)
        action.backgroundColor = .red
        
        let conf = UISwipeActionsConfiguration(actions: [action])
        conf.performsFirstActionWithFullSwipe = false
        return conf
    }
    
    private func setupAppearance() {
        tabBarController?.navigationItem.title = viewModel.title
    }
    
    private func updateUI() {
        let models = viewModel.models
        manager.memoryStorage.setItems(models)
    }
    
    //MARK: - Action
    
    private func open(user: User) {
        try! UserViewController
            .presenter()
            .performSourceConstruction()
            .presenter()
            .configure ({
                $0.viewModel = UserViewModel(user: user)
            })
            .push()
    }
}
