//
//  DataUserCell.swift
//  UsersCell
//
//  Created by Oleksandr Nesynov on 14.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift
import RxCocoa

final class DataUserTextField : UITextField {
    private var oldTextColor: UIColor!
    private var disposeBag = DisposeBag()
    
    //MARK: - IBInspectable

    @IBInspectable dynamic public var editingDidBeginColor: UIColor = .black
    @IBInspectable dynamic public var editingDidEnd: UIColor = .lightGray
    
    //MARK: - Lifecicle Object
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        if newSuperview != nil {
            setupObservers()
        } else {
            disposeBag = DisposeBag()
        }
    }
    
    //MARK: - Private Methods
    
    private func setupObservers() {
        rx.controlEvent([.editingDidBegin])
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.textColor = self?.editingDidBeginColor
            }).disposed(by: disposeBag)

        rx.controlEvent([.editingDidEnd])
            .asDriver()
            .drive(onNext: { [weak self] in
                self?.textColor = self?.editingDidEnd
            }).disposed(by: disposeBag)
    }
}
