//
//  UserViewController.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

import RxSwift
import RxCocoa
import DTTableViewManager

final class UserViewController: BaseViewController, DTTableViewManageable {
    @IBOutlet weak var tableView: UITableView!
    private var imagePickerManager: ImagePickerManager?
    
    var viewModel: UserViewModel? {
        didSet {
            observeViewModel()
            observeState()
            updateUI()
        }
    }
    
    //MARK: - Lifecicle Object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableManager()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        setupTabBarIndexWhenBack()
        setupAppearance()
    }
    
    //MARK: - Methods Public
    
    override func observeViewModel() {
        viewModel?.successHandler = { [weak self] in
            self?.updateUI()
        }
        
        viewModel?.errorHandler = { error in
            Alerts.defaultAlert(message: error.localizedDescription)
        }
    }
    
    override func observeState() {
        viewModel?.state.asDriver().drive(onNext: { [weak self] (state) in
            self?.view.progressFor(state: state)
        }).disposed(by: disposeBag)
    }
    
    //MARK: - Methods Private
    
    private func setupTableManager() {
        enableSelfSizing()
        manager.startManaging(withDelegate: self)
        manager.register(AvatarUserCell.self)
        manager.register(DataUserCell.self)
        manager.configure(AvatarUserCell.self) { (cell, _, _) in
            cell.changePhotoButton.rx.tap.subscribe(onNext: { [weak self] _ in
                self?.openImagePickerManager()
            }).disposed(by: cell.reuseDisposeBag)
        }
        manager.configure(DataUserCell.self) { [weak self] (cell, model, _) in
            self?.followInBox(cell: cell, model: model)
        }
    }
    
    private func followInBox(cell: DataUserCell, model: DataUserCellViewModelProtocol) {
        guard
            let model = model as? DataUserCellViewModel,
            let box = self.viewModel?.box
        else { return }
        
        switch model.type as? UserCellType {
        case .avatar?, nil: return
        case .firstName?: drive(cell: cell, to: box.firstName)
        case .lastName?: drive(cell: cell, to: box.lastName)
        case .email?: drive(cell: cell, to: box.email)
        case .phone?: drive(cell: cell, to: box.phone)
        }
    }
    
    private func drive(cell: DataUserCell, to driven: Variable<String?>) {
        cell.dataField.rx.value.asDriver().skip(1).drive(driven).disposed(by: cell.reuseDisposeBag)
    }
    
    private func openImagePickerManager() {
        self.imagePickerManager = ImagePickerManager(viewController: self,
                                                     callBack: writeUserImage)
    }
    
    private func writeUserImage(_ image: UIImage?) {
        self.imagePickerManager = nil
        image.map {
            viewModel?.write(image: $0, handler: { [weak self] isResult in
                guard isResult else { return }
                self?.updateUI(isLocal: true)
            })
        }
    }
    
    private func updateUI(isLocal: Bool = false) {
        guard let user = viewModel?.user else { return }
        manager.memoryStorage.removeAll()
        let models = UserTableViewContainable(user: user, isLocal: isLocal).models
        manager.memoryStorage.setItems(models)
    }
    
    private func setupTabBarIndexWhenBack() {
        let tabBarController = navigationController?.viewControllers.first as? TabBarController
        tabBarController?.selectedIndex = viewModel?.isUserModelSaved == true ? 1 : 0
    }
    
    private func setupAppearance() {
        navigationController?.navigationBar.items?.forEach {
            $0.title = Constants.NavigationController.titleBackButton
        }
    }
    
    //MARK: - Action
    
    @IBAction func saveTapped(_ sender: UIBarButtonItem) {
        //valid only European languages
        guard viewModel?.isValidUserName == true else {
            Alerts.defaultAlert(message: Localization.Alers.User.enterValidName)
            return
        }
        
        guard viewModel?.isValidEmail == true else {
            Alerts.defaultAlert(message: Localization.Alers.User.enterValidEmail)
            return
        }
        
        viewModel?.save()
        close()
    }
}
