//
//  TabBarController
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.12.17.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import UIKit
import Rswift

class TabBarController: UITabBarController {
    
    //MARK: - Lifecicle Object

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAppearance()
    }

    //MARK:- Private Method

    private func setupAppearance() {
        let firstItem = viewControllers?.first?.tabBarItem
    
        firstItem?.image = R.image.icon_tabbar_users()
        firstItem?.selectedImage = R.image.icon_tabbar_users_active()
        firstItem?.title = Localization.TabBarController.TabBarItem.users
        
        let lastItem = viewControllers?.last?.tabBarItem
        lastItem?.image = R.image.icon_tabbar_saved()
        lastItem?.selectedImage = R.image.icon_tabbar_saved_active()
        lastItem?.title = Localization.TabBarController.TabBarItem.saved
    }
}
