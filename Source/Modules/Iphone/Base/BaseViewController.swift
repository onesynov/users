//
//  BaseViewController.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift

class BaseViewController: UIViewController {
    let disposeBag = DisposeBag()

    //MARK: - Lifecicle Object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservers()
    }
    
    //MARK: Method (Overide this method if needed)
    
    func updateConstraintsIfNeeded() { }
    
    func observeViewModel() { }
    func observeNotification() { }
    func observeActions() { }
    func observeState() { }
    
    //MARK: Method

    private func setupObservers() {
        observeState()
        observeViewModel()
        observeNotification()
        observeActions()
    }
}
