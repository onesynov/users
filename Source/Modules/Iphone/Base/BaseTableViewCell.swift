//
//  BaseTableViewCell.swift
//  YBCMusic
//
//  Created by Oleksandr Nesynov on 21/9/17.
//  Copyright © 2017 MushketMobile. All rights reserved.
//

import RxSwift

class BaseTableViewCell: UITableViewCell {
    var reuseDisposeBag = DisposeBag()

    // MARK: - Lifecicle Object

    override func awakeFromNib() {
        super.awakeFromNib()
        setupProperties()
        selectionStyle = .none
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        reuseDisposeBag = DisposeBag()
    }

    // MARK: Method
    
    //This method are used for subclassing purposes only
    open func setupProperties() { }
}
