//
//  Constants.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

enum Constants {
    enum API {
        enum Header {
            static let accept = "Accept"
            static let contentType = "Content-Type"
            static let authorization = "Authorization"
        }
    }
    
    enum BaseView {
        static let errorfoundName = "Class don`t found with name"
        static let dot = "."
    }
    
    enum Users {
        static let animationDuration = 0.3
    }
    
    enum NavigationController {
        static let titleBackButton = " "
    }
}
