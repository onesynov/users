//
//  Alert.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

final class Alerts {
    static func present(alert: UIAlertController,
                        animated flag: Bool = true,
                        completion: (() -> Void)? = nil) {
        UIApplication.shared.keyWindow?.rootViewController?.present(alert,
                                                                    animated: flag,
                                                                    completion: completion)
    }
    
    class func defaultAlert(message: String) {
        if message.count == 0 { return }
        showAlert(Localization.Common.appName, message: message)
    }
    
    class func showAlert(_ title: String?, message: String) {
        if message.count == 0 { return }
        
        let defaultAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        defaultAlert.addAction(UIAlertAction(title: Localization.Common.ok, style: .cancel, handler: nil ))
        Alerts.present(alert: defaultAlert)
    }
}
