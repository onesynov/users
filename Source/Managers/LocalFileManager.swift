//
//  LocalFileManage.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

protocol TypeLocalUrlable {
    var documentsURL: URL { get }
    var folderURL: URL { get }
    var largeURL: URL { get }
    var thumbnailURL: URL { get }
    
    func data(_ quality: CGFloat) -> Data?
}

enum TypeLocalUrl: TypeLocalUrlable {
    
    case image(image: UIImage, userId: String)
    
    // MARK - Path URL
    
    var documentsURL: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Users")
    }
    
    var folderURL: URL {
        switch self {
        case let .image(_ , userId):
            return documentsURL.appendingPathComponent("id_\(userId)")
        }
    }
    
    var largeURL: URL {
        switch self {
        case let .image(_ , userId):
            return folderURL.appendingPathComponent("avatar_large\(userId)").appendingPathExtension("jpeg")
        }
    }
    
    var thumbnailURL: URL {
        switch self {
        case let .image(_ , userId):
            return folderURL.appendingPathComponent("avatar_thumbnail\(userId)").appendingPathExtension("jpeg")
        }
    }
    
    func data(_ quality: CGFloat) -> Data? {
        switch self {
            case let .image(image, _):
                return image.jpegData(compressionQuality: quality)
        }
    }
}

final class LocalFileManager {
    private lazy var queue: DispatchQueue = {
        return DispatchQueue(label: "com.usersapp.ios.read.write.file")
    }()
    
    // MARK: - Methods Public

    func writeImage(type: TypeLocalUrlable, handler: @escaping (Bool) -> ()) {
        var dataLarge: Data?
        var dataThumbnail: Data?

        
        let item = DispatchWorkItem {
            dataLarge = type.data(1.0)
            dataThumbnail = type.data(0.3)
        }
    
        item.notify(queue: .main) {
            guard let dataLarge = dataLarge,
                let dataThumbnail = dataThumbnail else { return }
            let isResultLarge = FileManager.write(data: dataLarge, path: type.largeURL)
            let isResultThumbnail = FileManager.write(data: dataThumbnail, path: type.thumbnailURL)
            handler(isResultLarge && isResultThumbnail)
        }
        
        queue.async(execute: item)
    }
    
    func retrieveImage(type: TypeLocalUrlable, handler: @escaping (UIImage?) -> ()) {
        var data: Data?
        var image: UIImage? //MARK: - TODO
        
        let itemRetrieveData = DispatchWorkItem {
            data = FileManager.retrieve(dataPath: type.largeURL)
        }
        
        let itemDataToImage = DispatchWorkItem {
            guard let data = data else { return }
            image = UIImage.init(data: data)
        }
        
        itemRetrieveData.notify(queue: queue, execute: itemDataToImage)
        
        itemDataToImage.notify(queue: .main) {
            handler(image)
        }
        
        queue.async(execute: itemRetrieveData)
    }
}
