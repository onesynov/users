//
//  ImagePickerManager.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import AVFoundation

final class ImagePickerManager: NSObject, UINavigationControllerDelegate {
    typealias SelectImageCallack = (UIImage?) -> ()
    
    private lazy var imagePicker: UIImagePickerController = {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        return imagePicker
    }()
    
    var viewController: UIViewController!
    var callBack: SelectImageCallack = {_ in }
    
    // MARK: - Lifecicle Object
    
    convenience init(viewController: UIViewController,
                     callBack: @escaping SelectImageCallack)
    {
        self.init()
        self.viewController = viewController
        self.callBack = callBack
        open()
    }
    
    deinit {
        print("---> deinit ImagePickerManager")
    }

    // MARK: - Private Methods
    
    private func openSettingsApp() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    private func showAlertDisablePermissions() { //MARK: - TODO: MOVE TO ALERTS
        let changeSettingsAlert = UIAlertController(title: "You do not have permissions enabled for this.",
                                                    message: "Would you like to change them in settings?",
                                                    preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.openSettingsApp()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.callBack(nil)
        }
        
        changeSettingsAlert.addAction(okAction)
        changeSettingsAlert.addAction(cancelAction)
        
        Alerts.present(alert: changeSettingsAlert)
    }
    
    // MARK: - Public Methods
    
    func open() {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        guard status != AVAuthorizationStatus.denied else {
            showAlertDisablePermissions()
            return
        }

        let alert = UIAlertController(title: "Where would you like to get photos from?",
                                      message: nil, //MARK: - TODO: MOVE TO ALERTS
                                      preferredStyle: .actionSheet)
        
        guard let view = viewController.view else { return }
        
        alert.popoverPresentationController?.sourceRect = view.bounds
        alert.popoverPresentationController?.sourceView = view
        
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .popover
        imagePicker.popoverPresentationController?.sourceView = view
        imagePicker.popoverPresentationController?.sourceRect = view.bounds
        
        
        //MARK: - TODO Need test on device
        let isCamera = UIImagePickerController.isSourceTypeAvailable(.camera)
        if isCamera { //MARK: - TODO: MOVE TO ALERTS
            let camera = UIAlertAction(title: "Take a Photo", style: .default) { (camera) -> Void in
                self.imagePicker.sourceType = .camera
                self.viewController.present(self.imagePicker, animated: true, completion: nil)
            }
            alert.addAction(camera)
        }
        
        let isPhotoLibrary = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        if isPhotoLibrary {
            let photoLibrary = UIAlertAction(title: "Choose from Library", style: .default) { _ in
                self.imagePicker.sourceType = .photoLibrary
                self.viewController.present(self.imagePicker, animated: true, completion: nil)
            }
            alert.addAction(photoLibrary)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.callBack(nil)
        }
        alert.addAction(cancelAction)
        
        Alerts.present(alert: alert)
    }
}

// MARK: - UIImagePickerControllerDelegate

extension ImagePickerManager: UIImagePickerControllerDelegate {
    typealias Info = UIImagePickerController.InfoKey
    
    private func finish(handler: () -> (UIImage?)) {
        viewController.view.hideProgress()
        callBack(handler())
        viewController.dismiss(animated: true, completion: nil)
    }
    
    @objc public func imagePickerController(_ picker: UIImagePickerController,
                                            didFinishPickingMediaWithInfo info: [Info: Any])
    {
        guard let chosenImage = info[Info.originalImage] as? UIImage else { return }
        finish { chosenImage }
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        finish { nil }
    }
}
