//
//  ContactsCell.swift
//  APIClient
//
//  Created by Oleksandr Nesynov on 14.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import Alamofire
import SwiftyJSON

class APIClient {
    @discardableResult
    private static func performRequest<T>(route: URLRequestConvertible,
                                          parseResponseHandler: @escaping (DataResponse<Any>) -> Result<T>,
                                          completion: @escaping (Result<T>) -> Void) -> DataRequest {
        return Alamofire
            .request(route)
            .responseJSON(queue: DispatchQueue.global(),
                          completionHandler: { response in
                                let responseParsed = parseResponseHandler(response)
                                DispatchQueue.main.async {
                                    completion(responseParsed)
                                }
            })
    }
    
    static func users(page: Int, results: Int, completion:@escaping (Result<[User]?>) -> Void) {
        let router = APIRouter.users(page: page, results: results)
        performRequest(route: router,parseResponseHandler: { response in
            switch response.result {
            case let .success(result): print(result)
                return .success(response.result.value
                .map { JSON($0) }
                .map { $0["results"].arrayValue }?
                .lazy
                .map { User(json: $0) })
            case let .failure(error): return .failure(error)
            }
        }, completion: completion)
    }
}
