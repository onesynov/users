//
//  StoreService.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 15.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RealmSwift

class StoreService {
    static let shared = StoreService()
    
    static func formatPredicate(user: User) -> String {
        return "uuid = '\(user.uuid)'"
    }
    
    private let labelQueue = "com.usersapp.ios.background"
    
    //MARK: Store Methods
    
    func store(users: [User]) {
        DispatchQueue(label: labelQueue).async {
            autoreleasepool {
                let realm = try! Realm()
                try! realm.write {
                    users.forEach {
                        let userObject = UserObject(user: $0)
                        realm.add(userObject, update: true)
                    }
                }
            }
        }
    }
    
    func store(user: User) {
        DispatchQueue(label: labelQueue).async {
            autoreleasepool {
                let realm = try! Realm()
                let userObject = UserObject(user: user)
                try! realm.write {
                    realm.add(userObject, update: true)
                }
            }
        }
    }
    
    //MARK: Restore Methods
    
    func users() -> [User]? {
        let realm = try! Realm()
        let objects = realm.objects(UserObject.self)
        let users = objects.map(User.init(object:)) as Array
        return users.count == 0 ? nil : users
    }
    
    func user(_ user: User) -> User? {
        let realm = try! Realm()
        return realm
            .objects(UserObject.self)
            .filter(StoreService.formatPredicate(user: user))
            .map(User.init(object:))
            .first
    }
    
    //MARK: Restore Methods
    
    func remove(user: User) {
        DispatchQueue(label: labelQueue).async {
            autoreleasepool {
                let realm = try! Realm()
                let objects = realm
                    .objects(UserObject.self)
                    .filter(StoreService.formatPredicate(user: user))
                try! realm.write {
                    objects.forEach { realm.delete($0) }
                }
            }
        }
    }
    
    func removeAllUsers() {
        DispatchQueue(label: labelQueue).async {
            autoreleasepool {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(realm.objects(UserObject.self))
                }
            }
        }
    }
}
