//
//  AppCommunicator.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 16.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import AppRouter

class AppCommunicator {
    static func setupRootViewController() {
        try! TabBarController
            .presenter()
            .performSourceConstruction()
            .presenter()
            .embedInNavigation()
            .configure { tabBar in
                let viewControllers = tabBar.viewControllers
                
                viewControllers?.first
                    .flatMap { $0 as? UsersViewController }
                    .map { $0.viewModel = UsersViewModel() }
                
                viewControllers?.last
                    .flatMap { $0 as? UsersViewController }
                    .map { $0.viewModel = UsersStoredViewModel(users: []) }}
            .setAsRoot()
    }
}
