//
//  UsersTableViewContainable.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 14.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

protocol UserTableViewContainableProtocol {
    var models: [AnyObject] { get }
}

protocol UserCellTypeProtocol {
    var title: String? { get }
    var data: String? { get }
}

enum UserCellType: UserCellTypeProtocol {
    case avatar
    case firstName(data: String?)
    case lastName(data: String?)
    case email(data: String?)
    case phone(data: String?)
    
    var title: String? {
        switch self {
        case .avatar: return nil
        case .firstName: return Localization.UserViewController.UserCell.Title.firstName
        case .lastName: return Localization.UserViewController.UserCell.Title.lastName
        case .email: return Localization.UserViewController.UserCell.Title.email
        case .phone: return Localization.UserViewController.UserCell.Title.phone
        }
    }
    
    var data: String? {
        switch self {
        case .avatar: return nil
        case .firstName(data: let data),
             .lastName(data: let data),
             .email(data: let data),
             .phone(data: let data): return data
        }
    }
}

final class UserTableViewContainable: UserTableViewContainableProtocol {
    private let user: User
    private let isLocal: Bool
    private lazy var orderCells: [UserCellType] = [.avatar,
                                                   .firstName(data: user.firstName),
                                                   .lastName(data: user.lastName),
                                                   .email(data: user.email),
                                                   .phone(data: user.phone)]
    
    var models: [AnyObject] {
        return modelsWith(types: orderCells, user: user)
    }

    required init(user: User, isLocal: Bool = false) {
        self.user = user
        self.isLocal = isLocal
    }
    
    fileprivate func modelsWith(types: [UserCellType], user: User) -> [AnyObject] {
        let localImagePath = TypeLocalUrl.image(image: UIImage(), userId: user.uuid).largeURL.absoluteString
        let imagePath = isLocal ? localImagePath : user.largeImagePath
        
        return types.map {
            switch $0 {
            case .avatar:
                return AvatarUserCellViewModel(imagePath: imagePath)
            case .firstName,
                 .lastName,
                 .email,
                 .phone:
                return DataUserCellViewModel(type: $0)
            }
        }
    }
}
