//
//  UserViewModel.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift
import Alamofire
import SwiftyJSON
import RealmSwift

protocol UserViewModelProtocol: class {
    var user: User { get }
    init(user: User)
}

final class UserViewModel: UserViewModelProtocol, ViewModelProtocol {
    var state: Variable<State> = Variable(.default)
    var successHandler: (() -> Void) = { }
    var errorHandler: ((Error) -> Void) = {_ in }
    var parameters: [String: Any] = [:]
    
    let box: UserBox
    
    private(set) var user: User {
        didSet {
            successHandler()
        }
    }

    var isValidEmail: Bool {
        return box.email.value?.isValidEmail ?? true
    }
    
    var isValidUserName: Bool {
        return [box.firstName, box.lastName]
            .lazy
            .compactMap { $0.value.map { $0.isValidUserName } }
            .reduce(true) { $0 && $1 }
    }
    
    private(set) var isUserModelSaved: Bool = false
    private var notificationToken: NotificationToken? = nil
    private let disposeBag = DisposeBag()
    
    //MARK: - Lifecicle Object

    required init(user: User) {
        self.user = user
        self.box = UserBox.init(user: user)
    }
    
    deinit {
        notificationToken?.invalidate()
    }

    //MARK: - Methods Public
    
    func save() {
        isUserModelSaved = true
        user.update(with: box)
        StoreService.shared.store(user: user)
    }

    func load() { }
    
    func write(image: UIImage, handler: @escaping (Bool) -> ()) {
        let type: TypeLocalUrl = .image(image: image, userId: user.uuid)
        LocalFileManager.init().writeImage(type: type) { [weak self] result in
            guard result else { return }
            
            self?.box.thumbnailImagePath.value = type.thumbnailURL.absoluteString
            self?.box.largeImagePath.value = type.largeURL.absoluteString
            
            handler(result)
        }
        
//        print("---->", type.thumbnailURL.absoluteString)
//        print("---->", type.largeURL.absoluteString)
//        print(image.debugDescription)
    }
}
