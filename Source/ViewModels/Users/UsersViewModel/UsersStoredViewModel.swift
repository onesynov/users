//
//  UsersViewModel.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift
import RealmSwift

protocol UsersStoredViewModelProtocol: class { }

final class UsersStoredViewModel: UsersViewModel, UsersStoredViewModelProtocol {
    private var tokenDeleted: NotificationToken? = nil
    private var tokenInserted: NotificationToken? = nil
    private var tokenModified: NotificationToken? = nil
    
    private(set) override var title: String {
        get { return Localization.UsersViewController.titleSaved }
        set { }
    }
    override var canLoad: Bool {
        get { return false }
        set { }
    }
    
    //MARK: - Lifecicle Object
    
    deinit {
        tokenDeleted?.invalidate()
        tokenInserted?.invalidate()
        tokenModified?.invalidate()
    }
    
    //MARK: - Method Public
    
    override func load() {
        users = StoreService.shared.users() ?? []
        successHandler()
    }
    
    override func configureRealmNotification() {
        let realm = try! Realm()
        
        tokenDeleted = realm.objects(UserObject.self).notify(when: .deleted) { [weak self] users in
            self?.successHandler()
        }
        
        tokenInserted = realm.objects(UserObject.self).notify(when: .inserted) { [weak self] users in
            self?.add(object: users)
        }
        
        tokenModified = realm.objects(UserObject.self).notify(when: .modified) { [weak self] users in
            self?.add(object: users)
        }
    }

    private func add(object: (Results<UserObject>)) {
        users = object.map(User.init(object:))
        successHandler()
    }
    
    func remove(user: User, at index: Int) {
        StoreService.shared.remove(user: user)
        users.remove(at: index)
    }
}
