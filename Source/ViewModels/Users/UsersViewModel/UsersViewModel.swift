//
//  UsersViewModel.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import RxSwift
import RealmSwift

protocol UsersViewModelProtocol: class {
    var title: String { get }
    var users: [User] { get }
    init(users: [User])
}

class UsersViewModel: UsersViewModelProtocol, ViewModelProtocol, ViewModelContainableProtocol, ViewModelPaginationProtocol {
    var state: Variable<State> = Variable(.default)
    var successHandler: (() -> Void) = { }
    var errorHandler: ((Error) -> Void) = { _ in }
    var parameters: [String: Any] = [:]
    
    private var token: NotificationToken? = nil
    private(set) var title = Localization.UsersViewController.titleUsers
    private var localStored: [User] = []
    
    var users: [User] = []
    
    var models: [UsersCellViewModel] {
        return users.map { UsersCellViewModel(user: $0) }
    }
    
    var page: Int = 1      
    var maxPage: Int = 20
    var result: Int = 20
    var canLoad: Bool = true
    
    //MARK: - Lifecicle Object
    
    required init(users: [User] = []) {
        self.users = users
        configureRealmNotification()
    }
    
//    deinit {
//        print("---<----DeInIt UsersViewModel ---->----")
//    }
    
    //MARK: - Methods Public
    
    open func load() {
        load(page: page, results: result)
    }
    
    open func configureRealmNotification() {
        let realm = try! Realm()
        
        token?.invalidate()
        token = nil
        
        token = realm.objects(UserObject.self).observe { [weak self] changes in
            guard let strongSelf = self else { return }
            
            switch changes {
            case .initial(let collectionType):
                
                self?.localStored = collectionType.map(User.init(object:))
                
            case let .update(collectionType, deletions: deletions, insertions: insertions, modifications: modifications):
                
                let usersDelete = deletions.map { strongSelf.localStored[$0] }
                
                let usersInsertion = insertions
                    .map { collectionType[$0] }
                    .map(User.init(object:))
                
                let usersModification = modifications
                    .map { collectionType[$0] }
                    .map(User.init(object:))
                
                self?.synchronizeStored(users: usersDelete)
                self?.synchronizeStored(users: usersInsertion)
                self?.synchronizeStored(users: usersModification)
                
                self?.localStored = collectionType.map(User.init(object:))
                self?.successHandler()
                
            case .error: Alerts.defaultAlert(message: Localization.Realm.errorNotification)
            }
        }
    }
    
    func willDisplay(row: Int) {
        guard (row == users.count - 2) && canLoad else { return }
        load()
    }
    
    func load(page: Int, results: Int) {
        state.value = .loading
        
        APIClient.users(page: page, results: results) { [weak self] result in
            self?.state.value = .`default`
            
            if case let .failure(error) = result {
                self?.errorHandler(error)
                return
            }
            
            guard case let .success(value) = result, let users = value else {
                self?.errorHandler(NSError.UsersApp.APIClient.serverError)
                return
            }
            
            self?.users.append(contentsOf: users)
            self?.successHandler()
            
            if self?.maxPage == self?.page { self?.canLoad = false }
            self?.page += 1
        }
    }
 
    private func synchronizeStored(users: [User]) {
        users.forEach { user in
            let uuidHashValue = user.uuid.hashValue
            self.users.firstIndex(where: { $0.uuid.hashValue == uuidHashValue })
                .map {
                    self.users.remove(at: $0)
                    self.users.insert(user, at: $0)
            }
        }
    }
}
