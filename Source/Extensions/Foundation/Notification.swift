//
//  Notification.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import Foundation

extension NotificationCenter {
    static func send(name: Notification.Name) {
        NotificationCenter.send(name: name, info: nil)
    }
    
    static func send(name: Notification.Name, info: [AnyHashable : Any]?) {
        NotificationCenter.default.post(name: name, object: nil, userInfo: info)
    }
    
    static func observe(_ observer: Any, selector aSelector: Selector, name aName: NSNotification.Name?) {
        NotificationCenter.default.addObserver(observer, selector: aSelector, name: aName, object: nil)
    }
}

extension Notification.Name {

}
