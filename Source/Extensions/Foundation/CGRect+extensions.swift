//
//  CGRect+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

extension CGRect {
    public init(w: CGFloat, h: CGFloat) {
        self.init(x: 0, y: 0, width: w, height: h)
    }
}
