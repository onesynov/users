//
//  UIView+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

extension UIView {
    class var identifier: String {
        let classIdentifier = NSStringFromClass(self)
        guard let indentifier = classIdentifier.lastComponent else { fatalError(Constants.BaseView.errorfoundName + classIdentifier) }
        return indentifier
    }
    var identifier: String {
        let classIdentifier = NSStringFromClass(type(of: self))
        guard let indentifier = classIdentifier.lastComponent else { fatalError(Constants.BaseView.errorfoundName + classIdentifier) }
        return indentifier
    }
    
    func roundCA(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        layer.masksToBounds = true
    }
    
    func roundAllCA() {
        let bounds = self.bounds
        let radius = max(bounds.height, bounds.width)
        roundCA(.allCorners, radius: radius)
    }
}

