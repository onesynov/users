//
//  UIView+LoadingView.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import MBProgressHUD

extension UIView {
    func showProgress() {
        hideProgress()
        MBProgressHUD.showAdded(to: self, animated: true)
    }
    
    func hideProgress() {
        MBProgressHUD.hide(for: self, animated: true)
    }
    
    func progressFor(state: State) {
        if state == .loading { showProgress() }
        else { hideProgress() }
    }
}
