//
//  DTTableViewManageable+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import DTTableViewManager

extension DTTableViewManageable {
    func enableSelfSizing(estimatedHeight: CGFloat = 60) {
        tableView.estimatedRowHeight = estimatedHeight
        tableView.rowHeight = UITableView.automaticDimension
    }
}
