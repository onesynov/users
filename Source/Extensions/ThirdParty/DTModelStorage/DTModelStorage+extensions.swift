//
//  DTModelStorage+extensions.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import DTModelStorage

extension MemoryStorage {
    open func removeAll() {
        startUpdate()
        removeAllItems()
        sections.removeAll()
        finishUpdate()
    }
}
