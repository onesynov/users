//
//  Model.swift
//  UsersApp
//
//  Created by Oleksandr Nesynov on 13.11.2018.
//  Copyright © 2018 Nesynov. All rights reserved.
//

import SwiftyJSON

protocol Model {
    init(json: JSON)
}
